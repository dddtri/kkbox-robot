Project Structure:
---root
	---bin  	        shell scripts for executing robot testcases
	---case 	        robot testcase files
	---src/main/python      python keywords
        ---src/unittest/python  python unit tests
	---target/robot_report	where test result report is generated


0. Prerequeste:
Python2.7 is installed and is set to PATH
pybuilder is pip installed and is set to PATH

1. Dependency Installation:
"pyb install_dependencies" will install the dependencies below:
robotframework (2.9.2)
robotframework-selenium2library (1.7.4)

2. Running Unit Test:
pyb run_unit_tests



3. Robot Testcase Execution:
   See shell scripts under bin folder for example.

