'''
Created on Oct 25, 2015

@author: daniel.shih
'''
import base64
class Base64Utility(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    
    @staticmethod
    def base64Decode(val):
        return base64.b64decode(val)
    
       
    @staticmethod
    def base64Encode(val):
        return base64.b64encode(val)