'''
Created on Oct 27, 2015

@author: daniel.shih
'''
from Selenium2Library import Selenium2Library
from selenium.common.exceptions import StaleElementReferenceException
import time
class CustomSeleniumLibrary(Selenium2Library):
    '''
    a library that introduces custom selenium2library keywords.  since it inherits selenium2library,
    it may leverages builtin keywords from it 
    '''


    def __init__(self):
        super(CustomSeleniumLibrary, self).__init__()
    
    def OnceElementIsVisibleShouldStayVisible(self, locator, period=10):
        """wait & verifies that element should have been visible & enabled for this period of time
        """
        self.wait_until_element_is_visible(locator, 5)

        self._info("Element'%s' should have been visible for '%s' seconds  ." % (locator, period))
        isState = True
        for i in range(0, int(period)) :
            try:
                isState = self.element_should_be_visible(locator) and self.element_should_be_enabled(locator)
                time.sleep(1)
            except AssertionError:
                isState = False
            finally:
                if isState is False :
                    raise AssertionError("Element'%s' should have been visible for '%s' seconds, but failed in '%s'th second" % (locator, period, i))
        
    def IfVisibleClickElement(self, locator, timeout=5):
        """if element is visible before timeout reaches, click it.  Otherwise, do nothing
        """
        try:
            self.wait_until_element_is_visible(locator, timeout)
        except AssertionError:
            return
        
        self._info("If element'%s' is visible within '%s' seconds, click it." % (locator, timeout))
        self.click_element(locator)
        
    def IfVisibleGetAlertMessage(self, dismiss=True):
        """
        If Alert doesn't exist, catch gracefully.  Otherwise, do the following:
        
        Returns the text of current JavaScript alert.

        By default the current JavaScript alert will be dismissed.
        This keyword will fail if no alert is present. Note that
        following keywords will fail unless the alert is
        dismissed by this keyword or another like `Get Alert Message`.
        """
        try:
            self.get_alert_message(dismiss)
        except Exception:
            return