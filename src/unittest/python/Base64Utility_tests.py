'''
Created on Oct 25, 2015

@author: root
'''
import unittest
import Base64Utility

class Base64UtilityTest(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testBase64Encoder(self):
        encoded = Base64Utility.Base64Utility.base64Encode("test123")
        self.assertEqual("test123", Base64Utility.Base64Utility.base64Decode(encoded))
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()