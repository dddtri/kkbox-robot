*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Base64Utility.py
Library			  CustomSeleniumLibrary.py

*** Variables ***
${GOOGLE URL}         https://www.kkbox.com/play/
${BROWSER}        firefox
${DELAY}          1



*** Keywords ***
Open Browser To Kkbox Page
    Open Browser    ${GOOGLE URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Kkbox Login Page Should Be Open

Kkbox Login Page Should Be Open
    Title Should Be    Let's music - KKBOX

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input User Name
    [Arguments]    ${userName}
    Input Text     uid   ${userName}
    
Input Password
    [Arguments]    ${passWord}
    ${plainPassWord} =  Base64 Decode  ${passWord}
    Input Text     pwd   ${plainPassWord}

Click Login
    Click Element   login-btn
    
    
    
Main Page Should Contain Left Panel
	Main Page Should Contain 我的音樂庫 Icon	
	Main Page Should Contain 線上精選 Icon
	Main Page Should Contain 電台 Icon
	Main Page Should Contain 一起聽 Icon
	
Main Page Should Contain 我的音樂庫 Icon
	Page Should Contain Element   xpath=//a[contains(@ng-click,"app.go('cpl', {})")]

Main Page Should Contain 線上精選 Icon
	Page Should Contain Element   xpath=//a[contains(@ng-click,"app.go('explore', {})")]

Main Page Should Contain 電台 Icon
	Page Should Contain Element   xpath=//a[contains(@ng-click,"app.go('radio', {})")]

Main Page Should Contain 一起聽 Icon
	Page Should Contain Element   xpath=//a[contains(@ng-click,"app.go('listen-with', {})")]
	
Login To Kkbox Web Player
    [Arguments]    ${userName}	${passWord}
    Open Browser To Kkbox Page
    Input User Name    ${userName}
    Input Password     ${passWord}
    Click Login
    Main Page Should Contain Left Panel
    
Input Song Search Field
	[Arguments]		${searchTerm}
    Input Text     xpath=//input[contains(@ng-keyup,"quickSearch()")]		${searchTerm}

Click Song Search Icon
    Click Element   search_btn_cnt
    
    
Song List Should Contain Row Where Column 歌曲名稱 And Column 歌手 Are
	[Arguments]		${songName}		${artist}
	Wait Until Element Is Visible	//div[contains(@class,"songs-table")]
	${actSongName} =	Get Table Cell		xpath=//div[contains(@class,"songs-table")]//table	3	2
	Should Be Equal		${actSongName}	${songName}
	${actArtist} =		Get Table Cell		xpath=//div[contains(@class,"songs-table")]//table	3	3
	Should Start With	${actArtist}	${artist}
	
Click First Radio Station Image In The List
	Wait Until Element Is Visible	promote-stations
	Execute Javascript    window.document.evaluate("//a[contains(@class,'btn-radio')][1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	${locator} =	Set Variable	xpath=//a[contains(@class,"btn-radio")][1]
	Mouse Over	${locator}
	Wait Until Element Is Visible	${locator}
	Click Element   ${locator}
	
If Visible Click Close Button In Advertisement Screen
	[Arguments]		${timeout}
	If Visible Click Element	xpath=//img[contains(@ng-click,"ads.closeModal()")]		${timeout}
Click Dislike Icon In Radio Station Panel
	Click Element	xpath=//a[contains(@ng-click,"dislikeFeedback()")]
	
Click 電台 Icon In Left Panel
	Click Element   xpath=//a[contains(@ng-click,"app.go('radio', {})")]
	
Once Pause Icon In Radio Station Panel Is Visible Should Stay Visible
	[Arguments]		${period}
	Once Element Is Visible Should Stay Visible	pauseBtn	${period}
	
Get Playing Song Text in Radio Station Panel
	${songName} =	Get Text	//div[contains(@id,"player")]/div[contains(@class,"title")]
	[Return]	${songName}