*** Settings ***
Documentation     A test suite that performs radio station dislike functionality
...
...               This test uses keywords created from imported resource
Resource          resource.robot

*** Test Cases ***
Radio Station Dislike
	Login To Kkbox Web Player		shenyushih@gmail.com	UUFaV1NYZWRj
	Click 電台 Icon In Left Panel
	Click First Radio Station Image In The List
	If Visible Get Alert Message
	If Visible Click Close Button In Advertisement Screen	10
	Once Pause Icon In Radio Station Panel Is Visible Should Stay Visible	5
	${currentSong} =	Get Playing Song Text in Radio Station Panel
	Click Dislike Icon In Radio Station Panel
	Once Pause Icon In Radio Station Panel Is Visible Should Stay Visible	5
	${nextSong} =	Get Playing Song Text in Radio Station Panel
	Should Not Be Equal		${currentSong}	${nextSong}
    [Teardown]    Close Browser
